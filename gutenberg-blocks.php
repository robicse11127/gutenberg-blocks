<?php
/**
 * Plugin Name: My Gutenberg Blocks
 * Description: A collection of Gutenberg Blocks
 * Author: MD. Rabiul Islam
 * Author URI: http://robizstory.com
 * Text-Domain: my-gutenberg-blocks
 */

if( ! defined( 'ABSPATH' ) ) : exit(); endif;

define( 'MYGBB_PLUGIN_URL', trailingslashit( plugins_url( '/', __FILE__ )));
define( 'MYGBB_PLUGIN_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );

/**
 * Register Gutenberg Sidebar
 */
add_action( 'enqueue_block_editor_assets', function() {
    wp_enqueue_script(
        'my-gutenberg-blocks',
        plugins_url( 'build/index.js', __FILE__ ),
        [ 'wp-plugins', 'wp-blocks', 'wp-editor', 'wp-edit-post', 'wp-i18n', 'wp-element', 'wp-components', 'wp-data' ]
    );
});

/**
 * Regsiter Front-end scripts
 */
add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_script(
        'mygbb-slick',
        plugins_url( 'assets/js/slick.min.js', __FILE__ ),
        [ 'jquery' ],
        rand(),
        false
    );
    wp_enqueue_script(
        'mygbb-owl',
        plugins_url( 'assets/js/owl.carousel.min.js', __FILE__ ),
        [ 'jquery' ],
        rand(),
        false
    );
    wp_enqueue_script(
        'mygbb-scripts',
        plugins_url( 'assets/js/scripts.js', __FILE__ ),
        [ 'jquery', 'mygbb-owl' ],
        rand(),
        true
    );


    /**
     * Styles
     */
    wp_enqueue_style( 'my-gutenberg-blocks-style', plugins_url( 'assets/css/style.css', __FILE__ ), [], false, 'all' );
} );

/**
 * Regsiter Editor Style
 */
add_action( 'admin_enqueue_scripts', function() {
    wp_register_script(
        'mygbb-owl',
        plugins_url( 'assets/js/owl.carousel.min.js', __FILE__ ),
        [ 'jquery' ],
        rand(),
        true
    );
    wp_register_script(
        'mygbb-slick',
        plugins_url( 'assets/js/slick.min.js', __FILE__ ),
        [ 'jquery' ],
        rand(),
        false
    );
    wp_register_script(
        'mygbb-editor',
        plugins_url( 'assets/js/editor.js', __FILE__ ),
        [ 'jquery', 'mygbb-owl' ],
        rand(),
        true
    );
    wp_enqueue_script( 'mygbb-owl' );
    wp_enqueue_script( 'mygbb-slick' );
    wp_enqueue_script( 'mygbb-editor' );

    wp_enqueue_style( 'my-gutenberg-blocks', plugins_url( 'assets/css/editor.css', __FILE__ ), [], false, 'all' );
});

/**
 * Register Blocks
 */
add_action( 'init', function() {
    register_block_type( 'my-gutenberg-blocks/blocks', [
        'style'         => 'my-gutenberg-blocks-style',
        'editor_style'  => 'my-gutenberg-blocks',
        'editor_script' => 'my-gutenberg-blocks',
    ] );
});

/**
 * Include all registered Blocks PHP ServerSide Scripts
 */
require_once MYGBB_PLUGIN_PATH . 'src/blocks/post-grid/post-grid.php';
require_once MYGBB_PLUGIN_PATH . 'src/blocks/post-carousel/post-carousel.php';
require_once MYGBB_PLUGIN_PATH . 'src/blocks/carousel/carousel.php';

/**
 * Require helper function
 */
require_once MYGBB_PLUGIN_PATH . 'inc/helpers.php';

