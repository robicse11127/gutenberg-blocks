import { registerBlockType } from "@wordpress/blocks"
import { __ } from "@wordpress/i18n"
import { select } from "@wordpress/data"
import {
    InspectorControls
} from "@wordpress/block-editor"

import {
    PanelBody,
    PanelRow,
    Button,
    SelectControl,
    TextControl,
    TextareaControl,
} from "@wordpress/components"

import domReady from '@wordpress/dom-ready';

const validAlignments = [ 'full' ]

registerBlockType( 'my-gutenberg-blocks/tabs', {
    title: __( 'Tab Block' ),
    icon: 'shield',
    category: 'common',
    keywords: [ 'Tab Block' ],
    attributes: {
        align: {
            type: 'string',
            default: 'full'
        },
        tabPanel: {
            type: 'array',
            default: [
                {
                    id: 'tab-panel-1',
                    name: 'Tab Panel 1',
                    content: 'Tab content goes here...'
                },
                {
                    id: 'tab-panel-2',
                    name: 'Tab Panel 2',
                    content: 'Tab content goes here...'
                }
            ]
        },
        blockId: {
            type: 'string'
        }
    },

     // Make the block full width by default
     getEditWrapperProps( attributes ) {
        const { align } = attributes
        if( -1 !== validAlignments.indexOf( align ) ) {
            return { 'data-align': align }
        }
    },


    /**
     * Edit Function
     */
    edit: ( { attributes, setAttributes, className, clientId } ) => {
        if( typeof jQuery !== 'undefined' ) {
            const $ = jQuery;

            var initMygbbTab = function( tabItem, tabItemContent ) {
                var parentClass    = '.mygbb-tab';
                var tabItem        = '.mygbb-tab-panel';
                var tabItemContent = '.mygbb-tab-content';
        
                $( parentClass ).each( function() {
                    var parentId              = clientId;
                    var targetSelector        = '#' + parentId + ' ' + tabItem;
                    var targetContentSelector = '#' + parentId + ' ' + tabItemContent
        
                    $( document.body ).on( 'click', targetSelector, function(e) {
                        e.preventDefault();
                        $( targetSelector ).removeClass( 'active' );
                        $( this ).addClass( 'active' );
                        var currentTabId = $( this ).data( 'tab-panel' );

                        var tabContentItems = $( targetContentSelector );
                        tabContentItems.each( function( index, item ) {
                            var contentId = $( this ).data( 'tab-content' );
                            if( currentTabId === contentId ) {
                                tabContentItems.removeClass( 'active' );
                                $( this ).addClass( 'active' );
                            }
                        } )
                    })
                } )
            };
            initMygbbTab();
        }

        const {
            tabPanel,
            blockId,
        } = attributes


        const setBlockId = ( blockId ) => setAttributes( { blockId } )

        if( clientId !== blockId ) {
            setBlockId( clientId )
        }

        /**
         * Add Tab Panel
         */
        const addTabPanel = () => {
            const tabPanelList = [ ...tabPanel ]
            tabPanelList.push({
                id: '',
                name: 'Tab Panel',
                content: 'Tab content goes here...'
            })
            setAttributes( { tabPanel: tabPanelList } )
        }

        /**
         * Remove Tab Panel
         */
        const removeTabPanel = ( index ) => {
            const tabPanelList = [ ...tabPanel ]
            tabPanelList.splice( index, 1 )
            setAttributes( { tabPanel: tabPanelList } )
        }

        /**
         * Update Tab Panel ID
         */
        const updateTabPanelID = ( id, index ) => {
            const tabPanelList = [ ...tabPanel ]
            tabPanelList[index].id = id
            setAttributes( { tabPanel: tabPanelList } )
        }

        /**
         * Update Tab Panel Name
         */
        const updateTabPanelName = ( name, index ) => {
            const tabPanelList = [ ...tabPanel ]
            tabPanelList[index].name = name
            setAttributes( { tabPanel: tabPanelList } )
        }

        /**
         * Update Tab Panel Content
         */
        const updateTabPanelContent = ( content, index ) => {
            const tabPanelList = [ ...tabPanel ]
            tabPanelList[index].content = content
            setAttributes( { tabPanel: tabPanelList } )
        }

        let tabPanelItems, tabPanelFields, tabContentItems;

        if( tabPanel.length ) {
            tabPanelItems = tabPanel.map( ( item, index ) => {
                let key = index + 1
                let className = ''
                if( key === 1 ) {
                    className = 'mygbb-tab-panel active'
                } else {
                    className = 'mygbb-tab-panel'
                }
                return(
                    <li key={ key } className={ className } data-tab-panel={ item.id }>
                        {item.name}
                    </li>
                )
            } )

            tabContentItems = tabPanel.map( ( item, index ) => {
                let key = index + 1
                let className = ''
                if( key === 1 ) {
                    className = 'mygbb-tab-content active'
                } else {
                    className = 'mygbb-tab-content'
                }
                return(
                    <div key={key} className={ className } data-tab-content={ item.id }>
                        { item.content }
                    </div>
                )
            } )

            tabPanelFields = tabPanel.map( ( item, index ) => {
                let key = index + 1
                return(
                    <PanelBody key={ key } title={ __( 'Tab Panel ' + key ) } initialOpen={ false }>

                        <TextControl
                            label={ __( 'Panel ID' ) }
                            value={ item.id }
                            onChange={ ( id ) => updateTabPanelID( id, index ) }/>

                        <TextControl
                            label={ __( 'Panel Name' ) }
                            value={ item.name }
                            onChange={ ( name ) => updateTabPanelName( name, index ) }/>

                        <TextareaControl
                            label={ __( 'Tab Content' ) }
                            value={ item.content }
                            onChange={ ( content ) => updateTabPanelContent( content, index ) }/>

                        <Button
                            className="components-button is-secondary"
                            onClick={ ( index ) => removeTabPanel( index ) }>
                                Remove
                        </Button>
                    </PanelBody>
                )
            } )
        }

        return(
            <>
                <InspectorControls>
                    <PanelBody title={ __( 'Tab Settings' ) } initialOpen={ false }>
                        { tabPanelFields }
                        <Button
                            className="components-button is-primary"
                            onClick={ addTabPanel.bind( this ) }>
                            Add New
                        </Button>
                    </PanelBody>
                </InspectorControls>
                <div className="mygbb-tab" id={ clientId }>
                    <ul className="mygbb-tab-panel-lists">
                        { tabPanelItems }
                    </ul>
                    <div className="tab-content-container">
                        { tabContentItems }
                    </div>
                </div>

            </>
        )
    },
    /**
     * Save Function
     */
    save: ( { attributes } ) => {
        const {
            tabPanel,
            blockId
        } = attributes
        console.log( blockId )
        let tabPanelItems, tabContentItems;

        if( tabPanel.length ) {
            tabPanelItems = tabPanel.map( ( item, index ) => {
                let key = index + 1
                let className = ''
                if( key === 1 ) {
                    className = 'mygbb-tab-panel active'
                } else {
                    className = 'mygbb-tab-panel'
                }
                return(
                    <li key={ key } className={ className } data-tab-panel={ item.id }>
                        {item.name}
                    </li>
                )
            } )

            tabContentItems = tabPanel.map( ( item, index ) => {
                let key = index + 1
                let className = ''
                if( key === 1 ) {
                    className = 'mygbb-tab-content active'
                } else {
                    className = 'mygbb-tab-content'
                }
                return(
                    <div key={key} className={ className } data-tab-content={ item.id }>
                        { item.content }
                    </div>
                )
            } )
        }
        return(
            <>
                <div className="mygbb-tab" id={ blockId }>
                    <ul className="mygbb-tab-panel-lists">
                        { tabPanelItems }
                    </ul>
                    <div className="tab-content-container">
                        { tabContentItems }
                    </div>
                </div>
            </>
        )
    }
} )