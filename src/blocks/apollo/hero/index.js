import { registerBlockType } from "@wordpress/blocks"
import { __ } from "@wordpress/i18n"
import {
    RichText,
    MediaUploadCheck,
    MediaUpload,
    InspectorControls
} from "@wordpress/block-editor"

import {
    PanelBody,
    PanelRow,
    Button,
    SelectControl
} from "@wordpress/components"

const validAlignments = [ 'full' ]

registerBlockType( 'my-gutenberg-blocks/apollo-hero', {
    title: __( 'Apollo Hero Block' ),
    icon: 'shield',
    category: 'common',
    keywords: [ 'Apollo Hero Block' ],
    attributes: {
        align: {
            type: 'string',
            default: 'full'
        },
        title: {
            type: 'string'
        },
        content: {
            type: 'string'
        },
        image: {
            type: 'string',
            default: ''
        },
        titleTag: {
            type: 'string',
            default: 'h2'
        }
    },

    // Make the block full width by default
    getEditWrapperProps( attributes ) {
        const { align } = attributes
        if( -1 !== validAlignments.indexOf( align ) ) {
            return { 'data-align': align }
        }
    },

    /**
     * Edit Function
     */
    edit: ( { attributes, setAttributes } ) => {
        const {
            title,
            content,
            image,
            titleTag
        } = attributes
        const ALLOWED_MEDIA_TYPES = [ 'image' ]

        return(
            <>
                <InspectorControls>
                    <PanelBody title={ __( "Settings" ) }>
                        <PanelRow>
                            <SelectControl
                                label={ __( 'Title Tag' ) }
                                value={ titleTag }
                                options={[
                                    { label: 'H1', value: 'h1' },
                                    { label: 'H2', value: 'h2' },
                                    { label: 'H3', value: 'h3' },
                                    { label: 'H4', value: 'h4' },
                                    { label: 'H5', value: 'h5' },
                                    { label: 'H6', value: 'h6' },
                                ]}
                                onChange={ ( newTitleTag ) => setAttributes( { titleTag: newTitleTag } ) }
                            />
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>

                <div id="hero">
                    <div className="wrapper">
                        <div className="welcome">
                            <RichText
                                key="title"
                                tagName={ titleTag }
                                placeholder="Apollo Jade Template"
                                value={ title }
                                onChange={ ( newTitle ) => setAttributes( { title: newTitle } ) }/>
                            <RichText
                                key="content"
                                tagName="p"
                                placeholder="Content goes here..."
                                value={ content }
                                onChange={ ( newContent ) => setAttributes( { content: newContent } ) }/>
                        </div>
                        <div className="photographer">
                            <MediaUploadCheck>
                                <MediaUpload
                                    onSelect={ ( media ) => setAttributes( { image: media.sizes.full.url } ) }
                                    allowedTypes={ ALLOWED_MEDIA_TYPES }
                                    value={ image }
                                    render={ ( { open } ) => (
                                        <Button
                                            onClick={ open }
                                            className="components-button is-secondary is-large"
                                            icon="upload">
                                            Upload Image
                                        </Button>
                                    ) }
                                />
                            </MediaUploadCheck>
                            { image !== '' &&
                                <Button
                                    onClick={ () => setAttributes( { image: '' } ) }
                                    className="components-button is-secondary is-large"
                                    icon="no-alt">
                                    Remove
                                </Button>
                            }
                            { image !== '' &&
                                <img src={ image } alt="Photographer" />
                            }
                        </div>
                    </div>
                </div>
            </>
        )
    },

    /**
     * Save Function
     */
    save: ( { attributes } ) => {
        const {
            title,
            content,
            image,
            titleTag
        } = attributes

        return(
            <>
                <div id="hero">
                    <div className="wrapper">
                        <div className="welcome">
                            <RichText.Content value={ title } tagName={ titleTag } />
                            <RichText.Content value={ content } tagName="p" />
                        </div>
                        <div className="photographer">
                            { image !== '' &&
                                <img src={ image } alt="Photographer" />
                            }
                        </div>
                    </div>
                </div>
            </>
        )
    },
} )