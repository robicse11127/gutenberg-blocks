import { registerBlockType } from "@wordpress/blocks"
import { __ } from "@wordpress/i18n"
import {
    RichText,
    MediaUploadCheck,
    MediaUpload,
    InspectorControls
} from "@wordpress/block-editor"

import {
    PanelBody,
    PanelRow,
    Button,
    SelectControl,
    TextControl
} from "@wordpress/components"

const validAlignments = [ 'full' ]

registerBlockType( 'my-gutenberg-blocks/apollo-intro', {
    title: __( 'Apollo Introduction Block' ),
    icon: 'shield',
    category: 'common',
    keywords: [ 'Apollo Intro Block' ],
    attributes: {
        align: {
            type: 'string',
            default: 'full'
        },
        title: {
            type: 'string'
        },
        content: {
            type: 'string'
        },
        image: {
            type: 'string',
            default: ''
        },
        titleTag: {
            type: 'string',
            default: 'h2'
        },
        socialIcons: {
            type: 'array',
            default: []
        }
    },

    // Make the block full width by default
    getEditWrapperProps( attributes ) {
        const { align } = attributes
        if( -1 !== validAlignments.indexOf( align ) ) {
            return { 'data-align': align }
        }
    },

    /**
     * Edit Function
     */
    edit: ( { attributes, setAttributes } ) => {
        const {
            title,
            content,
            image,
            titleTag,
            socialIcons
        } = attributes

        const ALLOWED_MEDIA_TYPES = [ 'image' ]

        /**
         * Add Social Icon
         */
        const addSocialIcon = () => {
            const iconList = [ ...socialIcons ]
            iconList.push({
                icon: '',
                link: ''
            })
            setAttributes( { socialIcons: iconList } )
        }

        /**
         * Remove Social Icon
         */
        const removeSocialIcon = ( index ) => {
            const iconList = [ ...socialIcons ]
            iconList.splice( index, 1 )
            setAttributes( { socialIcons: iconList } )
        }

        /**
         * Update Social Icon
         */
        const updateSocialIcon = ( icon, index ) => {
            const iconList = [ ...socialIcons ]
            iconList[index].icon = icon
            setAttributes( { socialIcons: iconList } )
        }

        /**
         * Update Social Icon Link
         */
        const updateSocialIconLink = ( link, index ) => {
            const iconList = [ ...socialIcons ]
            iconList[index].link = link
            setAttributes( { socialIcons: iconList } )
        }

        let socialIconItems, socialIconFields;

        if( socialIcons.length ) {
            socialIconItems = socialIcons.map( ( item, index ) => {
                return(
                    <>
                        <a href={ item.link }><i className={ item.icon }></i></a>
                    </>
                )
            } )

            socialIconFields = socialIcons.map( ( item, index ) => {
                return(
                    <>
                        <TextControl
                            label={ __( 'Icon' ) }
                            value={ item.icon }
                            onChange={ ( icon ) => updateSocialIcon( icon, index ) } />
                        <TextControl
                            label={ __( 'Link' ) }
                            value={ item.link }
                            onChange={ ( link ) => updateSocialIconLink( link, index ) } />
                        <Button
                            className="components-button is-secondary"
                            onClick={ () => removeSocialIcon( index )}>
                            Remove
                        </Button>
                        <hr/>
                    </>
                )
            } )
        }

        return(
            <>
                <InspectorControls>
                    <PanelBody title={ __( "Settings" ) }>
                        <PanelRow>
                            <SelectControl
                                label={ __( 'Title Tag' ) }
                                value={ titleTag }
                                options={[
                                    { label: 'H1', value: 'h1' },
                                    { label: 'H2', value: 'h2' },
                                    { label: 'H3', value: 'h3' },
                                    { label: 'H4', value: 'h4' },
                                    { label: 'H5', value: 'h5' },
                                    { label: 'H6', value: 'h6' },
                                ]}
                                onChange={ ( newTitleTag ) => setAttributes( { titleTag: newTitleTag } ) }
                            />
                        </PanelRow>
                    </PanelBody>
                    <PanelBody title={ __( 'Icon Settings' ) }>
                        { socialIconFields }
                        <Button
                            className="components-button is-primary"
                            onClick={ addSocialIcon.bind( this ) }>
                            Add New
                        </Button>
                    </PanelBody>
                </InspectorControls>

                <div id="about">
                    <div className="wrapper">
                        <div className="camera">
                            <MediaUploadCheck>
                                <MediaUpload
                                    onSelect={ ( media ) => setAttributes( { image: media.sizes.full.url } ) }
                                    allowedTypes={ ALLOWED_MEDIA_TYPES }
                                    value={ image }
                                    render={ ( { open } ) => (
                                        <Button
                                            onClick={ open }
                                            className="components-button is-secondary is-large"
                                            icon="upload">
                                            Upload Image
                                        </Button>
                                    ) }
                                />
                            </MediaUploadCheck>
                            { image !== '' &&
                                <Button
                                    onClick={ () => setAttributes( { image: '' } ) }
                                    className="components-button is-secondary is-large"
                                    icon="no-alt">
                                    Remove
                                </Button>
                            }
                            { image !== '' &&
                                <img src={ image } alt="Photographer" />
                            }
                        </div>
                        <div className="blurb">
                            <RichText
                                key="title"
                                placeholder="My name is Apollo"
                                tagName={ titleTag }
                                value={ title }
                                onChange={ ( newTitle ) => setAttributes( { title: newTitle } ) } />
                            <RichText
                                key="content"
                                placeholder="Content goes here..."
                                value={ content }
                                tagName="p"
                                onChange={ ( newContent ) => setAttributes( { content: newContent } ) } />
                            <div className="social">
                                { socialIconItems }
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    },

    /**
     * Save Function
     */
    save: ( { attributes } ) => {
        const {
            title,
            content,
            image,
            titleTag,
            socialIcons
        } = attributes

        let socialIconItems;

        if( socialIcons.length ) {
            socialIconItems = socialIcons.map( ( item, index ) => {
                return(
                    <>
                        <a href={ item.link }><i className={ item.icon }></i></a>
                    </>
                )
            } )
        }

        return(
            <>
                <div id="about">
                    <div className="wrapper">
                        <div class="camera">
                            { image !== '' &&
                                <img src={ image } alt="Camera" />
                            }
                        </div>
                        <div className="blurb">
                            <RichText.Content value={ title } tagName={ titleTag } />
                            <RichText.Content value={ content } tagName="p" />
                            <div className="social">
                                { socialIconItems }
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    },
} )