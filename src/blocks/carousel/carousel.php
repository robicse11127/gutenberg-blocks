<?php
/**
 * Page Grid Block JSX Renderer
 */
function mygbb_carsouel_jsx_rendered() {
    register_block_type( 'my-gutenberg-blocks/carousel', [
        'attributes'=> [
            'carousel' => [
                'type' => 'array',
                'default' => [
                    [
                        'quote' => 'A toast to taste',
                        'author' => 'John Doe'
                    ]
                ]
            ],
            'items' => [
                'type' => 'string',
                'default' => '3'
            ]
        ],
        'render_callback' => 'mygbb_carousel_render_callback'
    ] );
}
add_action( 'init', 'mygbb_carsouel_jsx_rendered' );

/**
 * Page Grid Block Renderer Callback Function
 */
function mygbb_carousel_render_callback( $attributes ) {

    // Gran the attributes values
    $carousel = $attributes['carousel'];
    $items = $attributes['items'];

    $data_attributes = [
        'data-items' => $items,
        'data-loop' => true
    ];
    ob_start();
    ?>
    <div class="owl-carousel mygbb-carousel-block" <?php echo mygbb_data_attributes( $data_attributes ); ?>>
        <?php foreach( $carousel as $slide ) : ?>
            <div class="item">
                <blockquote><?php echo $slide['quote']; ?></blockquote>
                <p><?php echo $slide['author']; ?></p>
            </div>
        <?php endforeach; ?>
    </div>
    <?php
    $content = ob_get_clean();
    return $content;
}

function mygbb_data_attributes( $attributes ) {
    $data_attributes_string = '';
    foreach( $attributes as $key=>$value ) {
        $data_attributes_string .= "{$key}='{$value}' ";
    }
    return $data_attributes_string;
}
