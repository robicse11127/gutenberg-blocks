import { registerBlockType } from "@wordpress/blocks"
import { __ } from "@wordpress/i18n"
import ServerSideRender from "@wordpress/server-side-render"
import {
    RichText,
    MediaUploadCheck,
    MediaUpload,
    InspectorControls
} from "@wordpress/block-editor"

import {
    PanelBody,
    PanelRow,
    Button,
    SelectControl,
    TextControl,
    TextareaControl,
} from "@wordpress/components"

import domReady from '@wordpress/dom-ready';

const validAlignments = [ 'full' ]

registerBlockType( 'my-gutenberg-blocks/carousel', {
    title: __( 'Carousel Block' ),
    icon: 'shield',
    category: 'common',
    keywords: [ 'Carousel Block' ],
    attributes: {
        align: {
            type: 'string',
            default: 'full'
        },
        carousel: {
            type: 'array',
            default: [
                {
                    quote: 'Carousel slide 1',
                    author: 'John Doe'
                },
                {
                    quote: 'Carousel slide 2',
                    author: 'John Doe'
                },
                {
                    quote: 'Carousel slide 3',
                    author: 'John Doe'
                },
            ]
        },
        items: {
            type: 'string',
            default: '3'
        },
    },

    // Make the block full width by default
    getEditWrapperProps( attributes ) {
        const { align } = attributes
        if( -1 !== validAlignments.indexOf( align ) ) {
            return { 'data-align': align }
        }
    },

    /**
     * Edit Function
     */
    edit: ( { attributes, setAttributes } ) => {

        const {
            carousel,
            items
        } = attributes

        domReady( function() {

        } );

        if( typeof jQuery !== "undefined" ) {
            const $ = jQuery;
            var initCarouselScript = function() {
                var owl = ".owl-carousel";
                var items = $( owl ).data( 'items' );
                $(".owl-carousel").owlCarousel({
                    loop: true,
                    items: items,
                    margin: 10,
                    nav: true,
                    center: false,
                    dots: true,
                    lazyLoad: true,
                    autoplay: false,
                    autoplaySpeed: false,
                    smartSpeed: 250,
                });
            }

            $( document.body ).on( 'click', function() {
                // $(".owl-carousel").owlCarousel('destroy')
                initCarouselScript();
            })

            $( document ).ready( function() {
                setTimeout( function() {
                    initCarouselScript();
                }, 1000 )
            } )
        }

        /**
         * Add Carousel
         */
        const addCarousel = () => {
            const carouselList = [ ...carousel ]
            carouselList.push({
                quote: 'An Awesome quote goes here',
                author: 'Author Name'
            })
            setAttributes( { carousel: carouselList } )
        }

        /**
         * Remove Carousel
         */
        const removeCarousel = ( index ) => {
            const carouselList = [ ...carousel  ]
            carouselList.splice( index, 1 );
            setAttributes( { carousel: carouselList } )
        }

        /**
         * Update Carousel Quote
         */
        const updateCarouselQuote = ( quote, index ) => {
            const carouselList = [ ...carousel  ]
            var currentCarosuel = Object.entries(carouselList[index]);
            currentCarosuel[0].splice( 1, 1, quote )
            carouselList[index] = Object.fromEntries( currentCarosuel )
            setAttributes( { carousel: carouselList } )
        }

        /**
         * Update Carousel Author
         */
        const updateCarouselAuthor = ( author, index ) => {
            const carouselList = [ ...carousel  ]
            var currentCarosuel = Object.entries(carouselList[index]);
            currentCarosuel[1].splice( 1, 1, author )
            carouselList[index] = Object.fromEntries( currentCarosuel )
            setAttributes( { carousel: carouselList } )
        }

        let carouselItems, carouselFields;

        if( carousel.length ) {
            carouselItems = carousel.map( ( item, index ) => {
                return(
                    <div key={index} className="item">
                        <blockquote>{ item.quote }</blockquote>
                        <p>{ item.author }</p>
                    </div>
                )
            } )

            carouselFields = carousel.map( ( item, index ) => {
                var key = index + 1;
                return(
                    <PanelBody key={key} title={ "Slider #" + key  } initialOpen={ false }>
                        <PanelRow>
                            <TextareaControl
                                label={ __( 'Quote' ) }
                                value={ item.quote }
                                onChange={ ( quote ) => updateCarouselQuote( quote, index ) }/>
                        </PanelRow>
                        <PanelRow>
                            <TextControl
                                label={ __( 'Author' ) }
                                value={ item.author }
                                onChange={ ( author ) => updateCarouselAuthor( author, index ) }/>
                        </PanelRow>
                        <PanelRow>
                            <Button
                                className="components-button is-secondary"
                                onClick={ () => removeCarousel( index ) }>
                                    Remove
                            </Button>
                        </PanelRow>
                    </PanelBody>
                )
            } )
        }

        return (
            <>
                <InspectorControls>
                    <PanelBody title={ __( 'General Settings' ) } initialOpen={ false }>
                        <TextControl
                            label={ __( 'Items' ) }
                            value={ items }
                            onChange={ ( value ) => setAttributes( { items: value } ) }/>
                    </PanelBody>
                    <PanelBody title={ __( 'Slider Settings' ) } initialOpen={ false }>
                        { carouselFields }
                        <Button
                            className="components-button is-primary"
                            onClick={ addCarousel.bind( this ) }>
                            Add New
                        </Button>
                    </PanelBody>
                </InspectorControls>
                <ServerSideRender
                    block="my-gutenberg-blocks/carousel"
                    attributes={{
                        carousel,
                        items
                    }}
                />
            </>
        )
    },

    /**
     * Save Function
     */
    save: ( { attributes } ) => {
        return null;
    }
} )