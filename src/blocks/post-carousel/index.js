import { registerBlockType } from "@wordpress/blocks"
import { __ } from "@wordpress/i18n"
import {
    RichText,
    MediaUploadCheck,
    MediaUpload,
    InspectorControls
} from "@wordpress/block-editor"

import {
    PanelBody,
    PanelRow,
    Button,
    SelectControl,
    TextControl,
} from "@wordpress/components"

import ServerSideRender from "@wordpress/server-side-render"

const validAlignments = [ 'full' ]

registerBlockType( 'my-gutenberg-blocks/post-carousel', {
    title: __( 'Post Carousel Block' ),
    icon: 'shield',
    category: 'common',
    keywords: [ 'Post Carousel Block' ],
    attributes: {
        align: {
            type: 'string',
            default: 'full'
        },
        perPage: {
            type: 'string',
            default: '9'
        },
        order: {
            type: 'string',
            default: 'DESC'
        }
    },

    // Make the block full width by default
    getEditWrapperProps( attributes ) {
        const { align } = attributes
        if( -1 !== validAlignments.indexOf( align ) ) {
            return { 'data-align': align }
        }
    },

    /**
     * Edit Function
     */
    edit: ( { attributes, setAttributes } ) => {

        if( typeof jQuery !== "undefined" ) {
            const $ = jQuery

            var owl   = ".post-carousel";
            var items = $( owl ).data( 'items' )

            $( ".post-carousel" ).owlCarousel({
                loop: true,
                items: 3,
                margin: 10,
                nav: true,
                center: false,
                dots: true,
                lazyLoad: true,
                autoplay: false,
                autoplaySpeed: false,
                smartSpeed: 250,
            });

            console.log( 'admin end loaded' )
        }

        const {
            perPage,
            order
        } = attributes
        return(
            <>
                <InspectorControls>
                    <PanelBody title={ __( 'Settings' ) }>
                        <PanelRow>
                            <TextControl
                                label={ __( 'Per Page' ) }
                                title={ __( 'Settings' ) }
                                value={ perPage }
                                onChange={ ( newPerPage ) => setAttributes( { perPage: newPerPage } ) }
                            />
                        </PanelRow>
                        <PanelRow>
                        <SelectControl
                                label="Order"
                                value={ order }
                                options={ [
                                    { label: 'DESC', value: 'DESC' },
                                    { label: 'ASC', value: 'ASC' },
                                ] }
                                onChange={ ( newOrder ) => setAttributes( { order: newOrder } ) }
                            />
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>
                <ServerSideRender
                    block="my-gutenberg-blocks/post-carousel"
                    attributes={{
                        perPage,
                        order
                    }}
                />
            </>
        )
    },

    /**
     * Save Function
     */
    save: () => {
        return null;
    }
} )