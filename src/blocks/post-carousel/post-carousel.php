<?php
/**
 * Page Grid Block JSX Renderer
 */
function mygbb_post_carousel_jsx_rendered() {
    register_block_type( 'my-gutenberg-blocks/post-carousel', [
        'attributes'=> [
            'perPage' => [
                'type' => 'string',
                'default' => '9'
            ],
            'order' => [
                'type' => 'string',
                'default' => 'DESC'
            ]
        ],
        'render_callback' => 'mygbb_post_carousel_render_callback'
    ] );
}
add_action( 'init', 'mygbb_post_carousel_jsx_rendered' );

/**
 * Page Grid Block Renderer Callback Function
 */
function mygbb_post_carousel_render_callback( $attributes ) {
    // Gran the attributes values
    $perPage = isset( $attributes['perPage'] ) ? intval( $attributes['perPage'] ) : 9;
    $order = isset( $attributes['order'] ) ? sanitize_text_field( $attributes['order'] ) : 'DESC';

    ob_start();
    $args = [
        'post_type'      => 'post',
        'posts_per_page' => $perPage,
        'post_status'    => 'publish',
        'order'          => $order,
        'order_by'       => ''
    ];
    $query = new WP_Query( $args );

    ?>
    <div class="my-gutenberg-block">
        <?php if( ! empty( $query->posts ) ) : ?>
            <div class="owl-carousel post-carousel">
                <?php foreach( $query->posts as $post ) : ?>
                    <div class="image-card">
                            <div class="image">
                                <?php
                                    $image_url = get_the_post_thumbnail_url( $post->ID, 'full' );
                                    echo "<img src='{$image_url}' alt='{$post->post_title}' />";
                                ?>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <h4><a href="<?php echo esc_url( get_the_permalink( $post->ID ) ); ?>"><?php echo mygbb_title( get_the_title( $post->ID ) ); ?></a></h4>
                                </div>
                                <div class="divider"></div>
                                <div class="excerpt">
                                    <p><?php echo mygbb_excerpt( get_the_excerpt( $post->ID ) ); ?></p>
                                </div>
                                <div class="readmore">
                                    <a href="<?php echo esc_url( get_the_permalink( $post->ID ) ); ?>" class="button button-readmore">Read More</a>
                                </div>
                            </div>
                        </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    <?php
    $content = ob_get_clean();
    return $content;
}