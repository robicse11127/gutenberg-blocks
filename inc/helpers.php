<?php
/**
 * Consists of some helper methods
 */

 /**
  * Excerpt Method
  */
function mygbb_excerpt( $str ) {
    if( strlen( $str ) > 100 ) {
        return $str = substr( $str, 0, 100 ) . '...';
    }
    return $str;
}

/**
 * Custom Title
 */
function mygbb_title( $title ) {
    if( strlen( $title ) > 35 ) {
        return $title = substr( $title, 0, 35 ) . '...';
    }
    return $title;
}