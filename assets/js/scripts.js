;(function($) {
    "use strict";

    /**
     * Carousel Block
     */
    var initCarouselBlockScript = function() {
        var owl   = ".mygbb-carousel-block";
        var items = $( owl ).data( 'items' )

        $( owl ).owlCarousel({
            loop: true,
            items: items,
            margin: 10,
            nav: true,
            center: false,
            dots: true,
            lazyLoad: true,
            autoplay: false,
            autoplaySpeed: false,
            smartSpeed: 250,
        });
    }
    initCarouselBlockScript();

    /**
     * Tab Block
     */
    var initMygbbTab = function( tabItem, tabItemContent ) {
        var parentClass    = '.mygbb-tab';
        var tabItem        = '.mygbb-tab-panel';
        var tabItemContent = '.mygbb-tab-content';

        $( parentClass ).each( function() {
            var parentId              = $( this ).attr( 'id' );
            var targetSelector        = '#' + parentId + ' ' + tabItem;
            var targetContentSelector = '#' + parentId + ' ' + tabItemContent

            $( document.body ).on( 'click', targetSelector, function(e) {
                e.preventDefault();
                $( targetSelector ).removeClass( 'active' );
                $( this ).addClass( 'active' );
                var currentTabId = $( this ).data( 'tab-panel' );
    
                var tabContentItems = $( targetContentSelector );
                tabContentItems.each( function( index, item ) {
                    var contentId = $( this ).data( 'tab-content' );
                    if( currentTabId === contentId ) {
                        tabContentItems.removeClass( 'active' );
                        $( this ).addClass( 'active' );
                    }
                } )
            })
        } )
    };
    initMygbbTab();

    /**
     * Post Carousel Block
     */
    var initPostCarouselBlockScript = function() {
        var owl   = ".post-carousel";
        var items = $( owl ).data( 'items' )

        $( ".post-carousel" ).owlCarousel({
            loop: true,
            items: 3,
            margin: 10,
            nav: true,
            center: false,
            dots: true,
            lazyLoad: true,
            autoplay: false,
            autoplaySpeed: false,
            smartSpeed: 250,
        });
    }
    initPostCarouselBlockScript();


})(jQuery);